var app = angular.module('App');

app.directive('projects', function() {
	
	var directive = {
        transclude: true,
		templateUrl: 'app/directive/projects.html'
	};
	
	return directive;
});