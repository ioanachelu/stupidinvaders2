var app = angular.module('App');

app.directive('statement', function() {
	
	var directive = {
        transclude: true,
		templateUrl: 'app/directive/statement.html'
            
	};
	
	return directive;
});