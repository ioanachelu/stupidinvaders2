var app = angular.module('App');

app.directive('contact', function() {
	
	var directive = {
		templateUrl: 'app/directive/contact.html'       
	};
	
	return directive;
});