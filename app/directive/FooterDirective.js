var app = angular.module('App');

app.directive('footer', function() {
	
	var directive = {
		templateUrl: 'app/directive/footer.html'       
	};
	
	return directive;
});