var app = angular.module('App');

app.directive('banner', function() {
	
	var directive = {
		templateUrl: 'app/directive/banner.html',
        transclude: true
	};
	
	return directive;
});